<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Demo Admin',
            'email' => 'admin@mail.com',
            'mobile' => '01712345677',
            'password' => bcrypt('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // supporter
        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Demo Supporter',
            'email' => 'supporter@mail.com',
            'mobile' => '01712345678',
            'password' => bcrypt('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // victim
        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Demo Victim',
            'email' => 'victim@mail.com',
            'mobile' => '01712345679',
            'password' => bcrypt('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
