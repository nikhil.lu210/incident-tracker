<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('incident_id')->unique();
            $table->string('title');

            $table->bigInteger('victim')
                  ->unsigned()
                  ->comment('Assign only those users whom role_id is 3');
            $table->foreign('victim')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade'); //Relation With Users Table

            $table->bigInteger('supporter')
                  ->unsigned()
                  ->nullable()
                  ->comment('Assign only those users whom role_id is 2');
            $table->foreign('supporter')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade'); //Relation With Users Table

            $table->timestamp('assigned_at')->nullable();

            $table->bigInteger('admin')
                  ->unsigned()
                  ->nullable()
                  ->comment('Assign only those users whom role_id is 1');
            $table->foreign('admin')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade'); //Relation With Users Table

            $table->text('description');
            $table->text('note')->nullable();
            // $table->string('latitude');
            // $table->string('longitude');
            $table->tinyInteger('status')
                  ->default(-1)
                  ->comment('-1 for pending, 0 for running and supporter assigned & 1 for completed');

            $table->timestamp('solved_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
        Schema::table("incidents", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
