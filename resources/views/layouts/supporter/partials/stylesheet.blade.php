<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" />

<link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">

@yield('css_links')
<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
{{-- Confirmation Alert --}}
<link rel="stylesheet" href="{{ asset('assets/css/confirmation_alert/jquery-confirmv3.3.2.min.css') }}">

<!-- Custom style CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

@yield('custom_css')
