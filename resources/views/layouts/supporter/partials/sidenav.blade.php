<!-- Side Nav START -->
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">
              <!-- <img alt="image" src="assets/img/logo.png" class="header-logo" /> -->
              <span class="logo-name">I T S</span>
            </a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture m-0">
                <img alt="image" src="{{ auth()->user()->avatar }}">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">{{ auth()->user()->name }}</div>
                <div class="user-role">{{ auth()->user()->role->name }}</div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>

            {{-- Dashboard Start --}}
            <li class="{{ Request::is('supporter') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('supporter.dashboard.index') }}">
                    <i data-feather="airplay"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            {{-- Dashboard Ends --}}

            <li class="menu-header">Incident</li>

            {{-- Incidents Starts --}}
            <li class="dropdown {{ Request::is('supporter/incident/*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle nav-link has-dropdown {{ Request::is('supporter/incident*') ? 'toggled' : '' }}">
                    <i data-feather="hash"></i>
                    <span>Incidents</span>
                </a>
                <ul class="dropdown-menu {{ Request::is('supporter/incident/all*') ? 'd-block' : '' }}">
                    <li class="{{ Request::is('supporter/incident/all*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('supporter.incident.index') }}">All Incidents</a>
                    </li>
                    
                    <li class="{{ Request::is('supporter/incident/new_assigned*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('supporter.incident.new_assigned') }}">New Assigned</a>
                    </li>

                    <li class="{{ Request::is('supporter/incident/solved*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('supporter.incident.solved') }}">Solved</a>
                    </li>
                </ul>
            </li>
            {{-- Incidents Ends --}}

        </ul>
    </aside>
</div>
<!-- Side Nav END -->
