<!-- Side Nav START -->
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">
              <!-- <img alt="image" src="assets/img/logo.png" class="header-logo" /> -->
              <span class="logo-name">I T S</span>
            </a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture m-0">
                <img alt="image" src="{{ auth()->user()->avatar }}">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">{{ auth()->user()->name }}</div>
                <div class="user-role">{{ auth()->user()->role->name }}</div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>

            {{-- Dashboard Start --}}
            <li class="{{ Request::is('victim') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('victim.dashboard.index') }}">
                    <i data-feather="airplay"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            {{-- Dashboard Ends --}}
            
            
            <li class="menu-header">Incidents</li>

            {{-- All Incident Start --}}
            <li class="{{ Request::is('victim/incident/all*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('victim.incident.index') }}">
                    <i data-feather="hash"></i>
                    <span>All Incident</span>
                </a>
            </li>
            {{-- All Incident Ends --}}

            {{-- New Incident Start --}}
            <li class="{{ Request::is('victim/incident/create*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('victim.incident.create') }}">
                    <i data-feather="share"></i>
                    <span>Create New Incident</span>
                </a>
            </li>
            {{-- New Incident Ends --}}

        </ul>
    </aside>
</div>
<!-- Side Nav END -->
