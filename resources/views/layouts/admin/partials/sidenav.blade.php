<!-- Side Nav START -->
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">
              <!-- <img alt="image" src="assets/img/logo.png" class="header-logo" /> -->
              <span class="logo-name">I T S</span>
            </a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture m-0">
                <img alt="image" src="{{ auth()->user()->avatar }}">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">{{ auth()->user()->name }}</div>
                <div class="user-role">{{ auth()->user()->role->name }}</div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>

            {{-- Dashboard Start --}}
            <li class="{{ Request::is('admin') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.dashboard.index') }}">
                    <i data-feather="airplay"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            {{-- Dashboard Ends --}}

            <li class="menu-header">Incident</li>

            {{-- Incidents Starts --}}
            <li class="dropdown {{ Request::is('admin/incident*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle nav-link has-dropdown {{ Request::is('admin/incident*') ? 'toggled' : '' }}">
                    <i data-feather="hash"></i>
                    <span>Incidents</span>
                </a>
                <ul class="dropdown-menu {{ Request::is('admin/incident/all*') ? 'd-block' : '' }}">
                    <li class="{{ Request::is('admin/incident/all*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.incident.index') }}">All Incidents</a>
                    </li>

                    <li class="{{ Request::is('admin/incident/pending*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.incident.pending') }}">Pending</a>
                    </li>

                    <li class="{{ Request::is('admin/incident/running*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.incident.running') }}">Running</a>
                    </li>

                    <li class="{{ Request::is('admin/incident/solved*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.incident.solved') }}">Solved</a>
                    </li>
                </ul>
            </li>
            {{-- Incidents Ends --}}

            <li class="menu-header">Users</li>

            {{-- Victim Start --}}
            <li class="{{ Request::is('admin/victim*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.victim.index') }}">
                    <i data-feather="users"></i>
                    <span>Victims</span>
                </a>
            </li>
            {{-- Victim Ends --}}


            {{-- Supporters Starts --}}
            <li class="dropdown {{ Request::is('admin/supporter*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle nav-link has-dropdown {{ Request::is('admin/supporter*') ? 'toggled' : '' }}">
                    <i data-feather="user-check"></i>
                    <span>Supporters</span>
                </a>
                <ul class="dropdown-menu {{ Request::is('admin/supporter/all*') ? 'd-block' : '' }}">
                    <li class="{{ Request::is('admin/supporter/all*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.supporter.index') }}">All Supporters</a>
                    </li>

                    <li class="{{ Request::is('admin/supporter/create*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.supporter.create') }}">Add Supporter</a>
                    </li>
                </ul>
            </li>
            {{-- Supporters Ends --}}


            {{-- Admins Starts --}}
            <li class="dropdown {{ Request::is('admin/administrator*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle nav-link has-dropdown {{ Request::is('admin/administrator*') ? 'toggled' : '' }}">
                    <i data-feather="user-plus"></i>
                    <span>Admins</span>
                </a>
                <ul class="dropdown-menu {{ Request::is('admin/administrator/all*') ? 'd-block' : '' }}">
                    <li class="{{ Request::is('admin/administrator/all*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.administrator.index') }}">All Admins</a>
                    </li>

                    <li class="{{ Request::is('admin/administrator/create*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.administrator.create') }}">Add Admin</a>
                    </li>
                </ul>
            </li>
            {{-- Admins Ends --}}

        </ul>
    </aside>
</div>
<!-- Side Nav END -->
