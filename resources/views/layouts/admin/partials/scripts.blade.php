<!-- General JS Scripts -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>

@yield('script_links')

<!-- Template JS File -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
{{-- Confirmation Alert --}}
<script src="{{ asset('assets/js/confirmation_alert/jquery-confirmv3.3.2.min.js') }}"></script>

@yield('custom_script')

<!-- Custom JS File -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/responsive.js') }}"></script>
