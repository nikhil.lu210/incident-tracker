<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <title>ADMINISTRATION | REGISTER</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">

    <!-- plugins css -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-social/bootstrap-social.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

    <style>
        .theme-white .form-control:focus {
            border-color: #191d21;
        }
    </style>
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="card card-dark">
                            <div class="card-header">
                                <h4>Register as Victim</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Full Name <sup class="text-danger"><b>*</b></sup></label>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" tabindex="1" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Ex: John Doe">

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number <sup class="text-danger"><b>*</b></sup></label>
                                        <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" tabindex="1" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus placeholder="Ex: 0987654321">

                                        @error('mobile')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email <sup class="text-danger"><b>*</b></sup></label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" tabindex="1" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Ex: user@mail.com">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password <sup class="text-danger"><b>*</b></sup></label>
                                        </div>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Ex: #12asASDasdf">
                                        <small>
                                            <b>Password Rules:</b>
                                            <ul>
                                                <li>English uppercase characters (A – Z)</li>
                                                <li>English lowercase characters (a – z)</li>
                                                <li>Base 10 digits (0 – 9)</li>
                                                <li>Non-alphanumeric (For example: !, $, #, or %)</li>
                                                <li>Unicode characters</li>
                                            </ul>
                                        </small>

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password-confirm" class="control-label">Confirm Password <sup class="text-danger"><b>*</b></sup></label>
                                        </div>
                                        <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" placeholder="Ex: #12asASDasdf">

                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-dark btn-lg btn-block">
                                            Register
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="mt-5 text-muted text-center">
                            Already have an account? <a href="{{ route('login') }}">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>











    <!-- General JS Scripts -->
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    <!-- JS Libraies -->
    <!-- Page Specific JS File -->
    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <!-- Custom JS File -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
