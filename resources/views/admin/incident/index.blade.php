@extends('layouts.admin.app')

@section('page_title', 'All Incident')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Incidents</li>
    <li class="breadcrumb-item active">All Incident</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    
    <div class="section-body">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-header">
                      <h4>All Incidents</h4>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-striped" id="table-1">
                              <thead>
                                  <tr>
                                      <th class="text-center">
                                          #
                                      </th>
                                      <th>ID</th>
                                      <th>Victim</th>
                                      <th>Supporter</th>
                                      <th>Created At</th>
                                      <th>Status</th>
                                      <th>Solved At</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach ($incidents as $sl => $incident)
                                    <tr>
                                        <td>{{ $sl+1 }}</td>
                                        <td>{{ $incident->incident_id }}</td>
                                        <td>
                                          <a href="{{ route('admin.victim.show', ['victim_id' => $incident->get_victim->id]) }}" target="_blank" class="text-bold text-dark">
                                            <b>{{ $incident->get_victim->name }}</b>
                                          </a>
                                        </td>
                                        <td>
                                          @if ($incident->get_supporter != NULL)
                                              <a href="{{ route('admin.supporter.show', ['supporter_id' => $incident->get_supporter->id]) }}" target="_blank" class="text-bold text-dark">
                                                  <b>{{ $incident->get_supporter->name }}</b>
                                              </a>
                                          @else
                                              <div class="badge badge-warning">Not Assigned</div>
                                          @endif
                                        </td>
                                        @php 
                                          $dd = new DateTime($incident->created_at); 
                                          $created_at = $dd->format('d M Y');
                                        @endphp
                                        <td>{{ $created_at }}</td>
                                        <td>
                                          @if ($incident->status == -1)
                                            <div class="badge badge-danger">Pending</div>
                                          @elseif($incident->status == 0)
                                            <div class="badge badge-warning">Running</div>
                                          @else
                                            <div class="badge badge-success">Solved</div>
                                          @endif
                                        </td>
                                        @php 
                                          if ($incident->solved_at != NULL) {
                                            $dd = new DateTime($incident->solved_at); 
                                            $solved_at = $dd->format('d M Y');
                                          }
                                        @endphp
                                        <td>
                                          @if ($incident->solved_at != NULL)
                                            {{ $solved_at }}
                                          @else
                                            <div class="badge badge-danger">Unsolved</div>
                                          @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.incident.show', ['page' => 'all', 'incident_id' => $incident->id]) }}" class="btn btn-dark btn-sm">Details</a>
                                        </td>
                                    </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>  
  


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
