@extends('layouts.admin.app')

@section('page_title', 'Incident Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/select2/dist/css/select2.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/bundles/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}"> --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Incidents</li>
    @if ($page == 'all')
        <li class="breadcrumb-item">
            <a href="{{ route('admin.incident.index') }}">All Incidents</a>
        </li>
    @elseif($page == 'pending')
        <li class="breadcrumb-item">
            <a href="{{ route('admin.incident.pending') }}">Pending Incidents</a>
        </li>
    @elseif($page == 'running')
        <li class="breadcrumb-item">
            <a href="{{ route('admin.incident.running') }}">Running Incidents</a>
        </li>
    @else
        <li class="breadcrumb-item">
            <a href="{{ route('admin.incident.solved') }}">Solved Incidents</a>
        </li>
    @endif
    <li class="breadcrumb-item active">Incident Details</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $incident->title }} <sup class="text-dark">( {{ $incident->incident_id }} )</sup></h4>
                    
                    @if ($incident->supporter == NULL)
                        <div class="card-header-action">
                            <button class="btn btn-dark btn-sm" type="button" data-toggle="modal"
                            data-target=".bd-example-modal-sm">Assign Supporter</button>
                        </div>

                        {{-- Modal Here --}}
                        @include('admin.incident.modals.assign_supporter')
                    @endif
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-responsive">
                        <thead>
                          <tr>
                            <th class="text-center">Victim</th>
                            <th class="text-center">Assigned Supporter</th>
                            <th class="text-center">Incident Received At</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Solved At</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center">
                                <a href="{{ route('admin.victim.show', ['victim_id' => $incident->get_victim->id]) }}" target="_blank" class="text-bold text-dark">
                                    <b>{{ $incident->get_victim->name }}</b>
                                </a>
                            </td>
                            <td class="text-center">
                                @if ($incident->get_supporter != NULL)
                                    <a href="{{ route('admin.supporter.show', ['supporter_id' => $incident->get_supporter->id]) }}" target="_blank" class="text-bold text-dark">
                                        <b>{{ $incident->get_supporter->name }}</b>
                                    </a>
                                @else
                                    <div class="badge badge-warning">Not Assigned</div>
                                @endif
                            </td>
                            @php 
                              $dd = new DateTime($incident->created_at); 
                              $created_at = $dd->format('d M Y');
                            @endphp
                            <td class="text-center">{{ $created_at }}</td>
                            <td class="text-center">
                                @if ($incident->status == -1)
                                    <div class="badge badge-danger">Pending</div>
                                @elseif($incident->status == 0)
                                    <div class="badge badge-warning">Running</div>
                                @else
                                    <div class="badge badge-success">Solved</div>
                                @endif
                            </td>
                            <td class="text-center">
                                @php 
                                  if ($incident->solved_at != NULL) {
                                    $dd = new DateTime($incident->solved_at); 
                                    $solved_at = $dd->format('d M Y');
                                  }
                                @endphp
                                @if ($incident->solved_at != NULL)
                                    {{ $solved_at }}
                                @else
                                    <div class="badge badge-danger">Unsolved</div>
                                @endif
                            </td>
                          </tr>
                        </tbody>
                    </table>

                    <hr>

                    <div class="row">
                        <div class="col-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h6>Incident Description</h6>
                                </div>
                                <div class="panel-body">
                                    {{-- {{ $incident->description }} --}}
                                    {!! $incident->description !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h6>Supporter Note</h6>
                                </div>
                                <div class="panel-body">
                                    {!! $incident->note !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    {{-- <script src="{{ asset('assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script> --}}
    <script src="{{ asset('assets/bundles/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('assets/js/page/create-post.js') }}"></script> --}}

    {{-- <script src="{{ asset('assets/js/page/forms-advanced-forms.js') }}"></script> --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
