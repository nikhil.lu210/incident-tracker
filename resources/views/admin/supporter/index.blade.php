@extends('layouts.admin.app')

@section('page_title', 'All Supporter')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Supporters</li>
    <li class="breadcrumb-item active">All Supporter</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    
    <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>All Supporters</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                      <tr>
                        <th class="text-center">
                        #
                        </th>
                        <th>Avatar</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Assigned Incident</th>
                        <th>Incident Solved</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                        <tbody>
                            @foreach ($supporters as $sl => $supporter)
                              <tr>
                                <td>{{ $sl+1 }}</td>
                                <td>
                                  <img alt="image" src="{{ $supporter->avatar }}" width="35">
                                </td>
                                <td>
                                    {{ $supporter->name }}
                                </td>
                                <td>{{ $supporter->email }}</td>
                                <td>{{ $supporter->mobile }}</td>
                                <td>{{ $supporter->supporters->where('supporter', $supporter->id)->count() }}</td>
                                <td>{{ $supporter->supporters->where('supporter', $supporter->id)->where('status', 1)->where('solved_at', !null)->count() }}</td>
                                <td>
                                    <a href="{{ route('admin.supporter.show', ['supporter_id' => $supporter->id]) }}" class="btn btn-dark btn-sm">Details</a>
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
