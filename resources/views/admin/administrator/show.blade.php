@extends('layouts.admin.app')

@section('page_title', 'Admin Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Admins</li>
    <li class="breadcrumb-item">
      <a href="{{ route('admin.administrator.index') }}">All Admins</a>
    </li>
    <li class="breadcrumb-item">Admin Details</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    
    <div class="section-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card author-box">
                    <div class="card-body">
                        <div class="author-box-center">
                            <img alt="image" src="{{ $admin->avatar }}" class="rounded-circle author-box-picture" />
                            <div class="clearfix"></div>
                            <div class="author-box-name">
                                <h4 class="text-bold">{{ $admin->name }}</h4>
                            </div>
                            <div class="author-box-job">{{ $admin->role->name }}</div>
                        </div>
                        <div class="py-1">
                            <p class="clearfix">
                                <span class="float-left">
                                    Email:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $admin->email }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Mobile:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $admin->mobile }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Assigned Incident:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $admin->admins->where('admin', $admin->id)->count() }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Solved Incident:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $admin->admins->where('admin', $admin->id)->where('status', 1)->where('solved_at', !null)->count() }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Joined At:
                                </span>
    
                                @php 
                                    $dd = new DateTime($admin->created_at); 
                                    $date = $dd->format('d-m-Y');
                                @endphp
    
                                <span class="float-right text-muted">
                                    {{ $date }}
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>All Incidents</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Incident ID</th>
                                        <th>Submitted At</th>
                                        <th>Solved At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($incidents as $sl => $incident)
                                    <tr>
                                        <td>{{ $sl+1 }}</td>
                                        <td>
                                            {{ $incident->incident_id }}
                                        </td>
    
                                        @php 
                                            $created_at = new DateTime($incident->created_at); $submitted = $created_at->format('d M Y'); 
                                            
                                            if ($incident->solved_at != NULL) { 
                                                $solved_at = new DateTime($incident->solved_at); 
                                                $solved = $solved_at->format('d M Y'); 
                                            } 
                                        @endphp
    
                                        <td>{{ $submitted }}</td>
                                        <td>
                                            @if ($incident->solved_at != NULL) {{ $solved }} @else
                                            <div class="badge badge-danger">Unsolved</div>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-dark btn-sm">Details</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
