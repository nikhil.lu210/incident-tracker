@extends('layouts.supporter.app')

@section('page_title', 'New Assigned Incidents')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Incidents</li>
    <li class="breadcrumb-item active">New Assigned Incidents</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    
    <div class="section-body">
      <div class="row">
          <div class="col-12">
              <div class="card">
                  <div class="card-header">
                      <h4>New Assigned Incidents</h4>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-striped" id="table-1">
                              <thead>
                                  <tr>
                                      <th class="text-center">
                                          #
                                      </th>
                                      <th>Incident ID</th>
                                      <th>Victim</th>
                                      <th>Created At</th>
                                      <th>Assigned At</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach ($incidents as $sl => $incident)
                                    <tr>
                                        <td>{{ $sl+1 }}</td>
                                        <td>{{ $incident->incident_id }}</td>
                                        <td><b>{{ $incident->get_victim->name }}</b></td>
                                        @php 
                                          $dd = new DateTime($incident->created_at); 
                                          $created_at = $dd->format('d M Y');
                                        @endphp
                                        <td>{{ $created_at }}</td>
                                        @php 
                                          $dd = new DateTime($incident->assigned_at); 
                                          $assigned_at = $dd->format('d M Y');
                                        @endphp
                                        <td>{{ $assigned_at }}</td>
                                        <td>
                                            <a href="{{ route('supporter.incident.show', ['page' => 'new_assigned', 'incident_id' => $incident->id]) }}" class="btn btn-dark btn-sm">Details</a>
                                        </td>
                                    </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>  
  


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
