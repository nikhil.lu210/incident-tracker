<!-- Small Modal -->
<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mySmallModalLabel">Mark As Solved</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('supporter.incident.mark_as_solved', ['incident_id' => $incident->id]) }}" method="post">
                    @csrf
                    <div class="form-group" style="display: grid">
                        <label>Write Note <span class="text-danger">*</span></label>
                        <textarea name="note" id="note" class="form-control" style="border-radius: 5px;" rows="3" placeholder="Write a short note." required></textarea>
                    </div>

                    <button class="btn btn-dark btn-block" type="submit" style="border-radius: 5px; padding: 10px 15px;" onclick="return confirm('Are You Sure?');">Mark As Solved</button>
                </form>
            </div>
        </div>
    </div>
</div>
