@extends('layouts.supporter.app')

@section('page_title', 'Dashboard')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Dashboard</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    <section class="section">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-statistic-4">
                        <div class="info-box7-block">
                            <h6 class="m-b-20 text-right">Total Incident</h6>
                            <h4 class="text-right">
                              <i class="fas fa-bolt pull-left bg-indigo c-icon"></i>
                              <span>{{ $total_incident }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-statistic-4">
                        <div class="info-box7-block">
                            <h6 class="m-b-20 text-right">Total Assigned</h6>
                            <h4 class="text-right">
                                <i class="fas fa-stamp pull-left bg-cyan c-icon"></i>
                                <span>{{ $total_assigned }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-statistic-4">
                        <div class="info-box7-block">
                            <h6 class="m-b-20 text-right">Total Active</h6>
                            <h4 class="text-right">
                                <i class="fas fa-arrow-up pull-left bg-warning c-icon"></i>
                                <span>{{ $total_active }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-statistic-4">
                        <div class="info-box7-block">
                            <h6 class="m-b-20 text-right">Total Solved</h6>
                            <h4 class="text-right"><i class="fas fa-check pull-left bg-success c-icon"></i><span>{{ $total_solved }}</span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
