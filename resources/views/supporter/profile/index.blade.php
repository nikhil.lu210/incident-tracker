@extends('layouts.supporter.app')

@section('page_title', 'Profile')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">Profile</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    <div class="section-body">
        <div class="row mt-sm-4">
            <div class="col-md-4">
                <div class="card author-box">
                    <div class="card-body">
                        <div class="author-box-center">
                            <img alt="image" src="{{ $supporter->avatar }}" class="rounded-circle author-box-picture" />
                            <div class="clearfix"></div>
                            <div class="author-box-name">
                                <h4 class="text-bold">{{ $supporter->name }}</h4>
                            </div>
                            <div class="author-box-job">{{ $supporter->role->name }}</div>
                        </div>
                        <div class="py-1">
                            <p class="clearfix">
                                <span class="float-left">
                                    Email:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $supporter->email }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Mobile:
                                </span>
                                <span class="float-right text-muted">
                                    {{ $supporter->mobile }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Joined At:
                                </span>

                                @php
                                    $dd = new DateTime($supporter->created_at);
                                    $date = $dd->format('d-m-Y');
                                @endphp
                                
                                <span class="float-right text-muted">
                                    {{ $date }}
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>Change Password</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('supporter.profile.update.password', ['supporter_id' => encrypt($supporter->id)]) }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-12 col-12">
                                    <label>Old Password <sup class="text-danger">*</sup></label>
                                    <input type="password" name="old_password" class="form-control" placeholder="Old Password" required/>
                                </div>
                                <div class="form-group col-md-12 col-12">
                                    <label>New Password <sup class="text-danger">*</sup></label>
                                    <input type="password" name="password" class="form-control" placeholder="New Password" required/>
                                    <small>
                                        <b>Password Rules:</b>
                                        <ul>
                                            <li>English uppercase characters (A – Z)</li>
                                            <li>English lowercase characters (a – z)</li>
                                            <li>Base 10 digits (0 – 9)</li>
                                            <li>Non-alphanumeric (For example: !, $, #, or %)</li>
                                            <li>Unicode characters</li>
                                        </ul>
                                    </small>
                                </div>
                                <div class="form-group col-md-12 col-12">
                                    <label>Confirm Password <sup class="text-danger">*</sup></label>
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required/>
                                </div>
                                <div class="form-group col-md-12 col-12">
                                    <button class="btn btn-dark btn-block" type="submit">Update Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Update Profile Info</h4>
                    </div>
                    <form action="{{ route('supporter.profile.update', ['supporter_id' => $supporter->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Avatar</label>
                                <div class="col-sm-12 col-md-7">
                                    <div id="image-preview" class="image-preview">
                                        <label for="image-upload" id="image-label">Choose File</label>
                                        <input type="file" name="avatar" id="image-upload" />
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name <sup class="text-danger">*</sup></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" required placeholder="supporter Name" value="{{ $supporter->name }}" name="name" class="form-control">
                                </div>
                            </div>
    
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email <sup class="text-danger">*</sup></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="email" required placeholder="supporter Login Email" value="{{ $supporter->email }}" name="email" class="form-control">
                                </div>
                            </div>
    
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mobile <sup class="text-danger">*</sup></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" required placeholder="supporter Mobile Number" value="{{ $supporter->mobile }}" name="mobile" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-dark float-right m-b-15">Update Information</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/create-post.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
