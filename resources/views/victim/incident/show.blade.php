@extends('layouts.victim.app')

@section('page_title', 'Incident Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/select2/dist/css/select2.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/bundles/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}"> --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('breadcrumb_section')
    <li class="breadcrumb-item">
        <a href="{{ route('victim.incident.index') }}">All Incidents</a>
    </li>
    <li class="breadcrumb-item active">Incident Details</li>
@endsection

@section('main_content')
    {{-- ========================================================================
    ============================< Main Section Starts >==========================
    ======================================================================== --}}


    <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $incident->title }} <sup class="text-dark">( {{ $incident->incident_id }} )</sup></h4>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-responsive">
                        <tbody>
                          <tr>
                            <th>Incident ID</th>
                            <td>{{ $incident->incident_id }}</td>
                          </tr>
                          
                          <tr>
                            <th>Incident Title</th>
                            <td>{{ $incident->title }}</td>
                          </tr>
                          
                          <tr>
                            <th>Supporter</th>
                            <td>
                                @if ($incident->supporter == !null)
                                    {{ $incident->get_supporter->name }}
                                @else
                                    <div class="badge badge-primary">Supporter Not Assigned</div>
                                @endif
                            </td>
                          </tr>
                          
                          <tr>
                            <th>Supporter Mobile</th>
                            <td>
                                @if ($incident->supporter == !null)
                                    {{ $incident->get_supporter->mobile }}
                                @else
                                    <div class="badge badge-primary">Supporter Not Assigned</div>
                                @endif
                            </td>
                          </tr>
                          
                          <tr>
                            <th>Supporter Email</th>
                            <td>
                                @if ($incident->supporter == !null)
                                    {{ $incident->get_supporter->email }}
                                @else
                                    <div class="badge badge-primary">Supporter Not Assigned</div>
                                @endif
                            </td>
                          </tr>
                          
                          <tr>
                            @php 
                                $dd = new DateTime($incident->created_at); 
                                $created_at = $dd->format('d M Y');
                            @endphp
                            <th>Incident Arrived</th>
                            <td>{{ $created_at }}</td>
                          </tr>
                          
                          <tr>
                            @php 
                                if ($incident->assigned_at == !null) {
                                    $dd = new DateTime($incident->assigned_at); 
                                    $assigned_at = $dd->format('d M Y');
                                }
                            @endphp
                            <th>Supporter Assigned At</th>                            
                            <td>
                                @if ($incident->supporter == !null)
                                    {{ $assigned_at }}
                                @else
                                    <div class="badge badge-primary">Supporter Not Assigned</div>
                                @endif
                            </td>
                          </tr>
                          
                          <tr>
                            <th>Solved At</th>
                            <td>
                                @php 
                                    if ($incident->solved_at != NULL) {
                                        $dd = new DateTime($incident->solved_at); 
                                        $solved_at = $dd->format('d M Y');
                                    }
                                @endphp
                                @if ($incident->solved_at != NULL)
                                    {{ $solved_at }}
                                @else
                                    <div class="badge badge-danger">Unsolved</div>
                                @endif
                            </td>
                          </tr>
                          
                          <tr>
                            <th>Status</th>
                            <td>
                                @if ($incident->status == -1)
                                    <div class="badge badge-danger">Pending</div>
                                @elseif($incident->status == 0)
                                    <div class="badge badge-warning">Running</div>
                                @else
                                    <div class="badge badge-success">Solved</div>
                                @endif
                            </td>
                          </tr>
                        </tbody>
                    </table>

                    <hr>

                    <div class="panel">
                        <div class="panel-heading">
                            <h6>Incident Description</h6>
                        </div>
                        <div class="panel-body">
                            {{-- {{ $incident->description }} --}}
                            {!! $incident->description !!}
                        </div>
                    </div>

                    @if ($incident->supporter != NULL && $incident->note != NULL)
                        <hr>
                        <div class="panel">
                            <div class="panel-heading">
                                <h6>Supporter Note</h6>
                            </div>
                            <div class="panel-body">
                                {!! $incident->note !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
          </div>
        </div>
      </div>


    {{-- ========================================================================
    =============================< Main Section Ends >===========================
    ======================================================================== --}}
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <!-- JS Libraies -->
    {{-- <script src="{{ asset('assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script> --}}
    <script src="{{ asset('assets/bundles/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('assets/js/page/create-post.js') }}"></script> --}}

    {{-- <script src="{{ asset('assets/js/page/forms-advanced-forms.js') }}"></script> --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
