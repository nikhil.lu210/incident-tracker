<?php

use Illuminate\Support\Facades\Route;

// victim Routes
Route::group([
    'prefix' => 'victim', // URL
    'as' => 'victim.', // Route
    'namespace' => 'Victim', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // incident
        include_once 'incident/incident.php';
        // profile
        include_once 'profile/profile.php';
    }
);
