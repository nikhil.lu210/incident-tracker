<?php

use Illuminate\Support\Facades\Route;

// Profile
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::get('', 'ProfileController@index')->name('index');
        Route::post('/update/{victim_id}', 'ProfileController@update')->name('update');
        Route::post('/update_password/{victim_id}', 'ProfileController@passwordUpdate')->name('update.password');
    }
);
