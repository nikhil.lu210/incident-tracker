<?php

use Illuminate\Support\Facades\Route;

// Incident
Route::group([
    'prefix' => 'incident', //URL
    'as' => 'incident.', //Route
    'namespace' => 'Incident', // Controller
],
    function(){
        Route::get('/all', 'IncidentController@index')->name('index');
        Route::get('/create', 'IncidentController@create')->name('create');
        Route::post('/store', 'IncidentController@store')->name('store');

        Route::get('/details/{incident_id}', 'IncidentController@show')->name('show');
    }
);
