<?php

use Illuminate\Support\Facades\Route;

// supporter Routes
Route::group([
    'prefix' => 'supporter', // URL
    'as' => 'supporter.', // Route
    'namespace' => 'Supporter', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // incident
        include_once 'incident/incident.php';
        // profile
        include_once 'profile/profile.php';
    }
);
