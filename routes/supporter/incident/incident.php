<?php

use Illuminate\Support\Facades\Route;

// Incident
Route::group([
    'prefix' => 'incident', //URL
    'as' => 'incident.', //Route
    'namespace' => 'Incident', // Controller
],
    function(){
        Route::get('/new_assigned', 'IncidentController@newAssigned')->name('new_assigned');
        Route::get('/all', 'IncidentController@index')->name('index');
        Route::get('/solved', 'IncidentController@solved')->name('solved');

        Route::get('/{page}/details/{incident_id}', 'IncidentController@show')->name('show');

        Route::post('/mark_as_solved/{incident_id}', 'IncidentController@mark_as_solved')->name('mark_as_solved');
    }
);
