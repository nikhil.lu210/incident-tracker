<?php

use Illuminate\Support\Facades\Route;

// Victim
Route::group([
    'prefix' => 'victim', //URL
    'as' => 'victim.', //Route
    'namespace' => 'Victim', // Controller
],
    function(){
        Route::get('/all', 'VictimController@index')->name('index');
        Route::get('/all/details/{victim_id}', 'VictimController@show')->name('show');
    }
);
