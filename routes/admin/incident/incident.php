<?php

use Illuminate\Support\Facades\Route;

// Incident
Route::group([
    'prefix' => 'incident', //URL
    'as' => 'incident.', //Route
    'namespace' => 'Incident', // Controller
],
    function(){
        Route::get('/all', 'IncidentController@index')->name('index');
        Route::get('/pending', 'IncidentController@pending')->name('pending');
        Route::get('/running', 'IncidentController@running')->name('running');
        Route::get('/solved', 'IncidentController@solved')->name('solved');
        
        Route::get('/{page}/details/{incident_id}', 'IncidentController@show')->name('show');

        Route::post('/assign_supporter/{incident_id}', 'IncidentController@assign_supporter')->name('assign_supporter');
    }
);
