<?php

use Illuminate\Support\Facades\Route;

// Administrator
Route::group([
    'prefix' => 'administrator', //URL
    'as' => 'administrator.', //Route
    'namespace' => 'Administrator', // Controller
],
    function(){
        Route::get('/all', 'AdministratorController@index')->name('index');
        Route::get('/create', 'AdministratorController@create')->name('create');
        Route::post('/store', 'AdministratorController@store')->name('store');
        Route::get('/all/details/{admin_id}', 'AdministratorController@show')->name('show');
    }
);
