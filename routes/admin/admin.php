<?php

use Illuminate\Support\Facades\Route;

// Admin Routes
Route::group([
    'prefix' => 'admin', // URL
    'as' => 'admin.', // Route
    'namespace' => 'Admin', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // incident
        include_once 'incident/incident.php';
        // victim
        include_once 'victim/victim.php';
        // supporter
        include_once 'supporter/supporter.php';
        // administrator
        include_once 'administrator/administrator.php';
        // profile
        include_once 'profile/profile.php';
    }
);
