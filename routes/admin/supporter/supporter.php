<?php

use Illuminate\Support\Facades\Route;

// Supporter
Route::group([
    'prefix' => 'supporter', //URL
    'as' => 'supporter.', //Route
    'namespace' => 'Supporter', // Controller
],
    function(){
        Route::get('/all', 'SupporterController@index')->name('index');
        Route::get('/create', 'SupporterController@create')->name('create');
        Route::post('/store', 'SupporterController@store')->name('store');
        Route::get('/all/details/{supporter_id}', 'SupporterController@show')->name('show');
    }
);
