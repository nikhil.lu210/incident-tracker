<?php

use Illuminate\Support\Facades\Route;

// Profile
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::get('/all', 'ProfileController@index')->name('index');
        Route::post('/update/{admin_id}', 'ProfileController@update')->name('update');
        Route::post('/update_password/{admin_id}', 'ProfileController@passwordUpdate')->name('update.password');
    }
);
