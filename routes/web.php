<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'Admin\Dashboard\DashboardController@index');


/*===================================
===========< Admin Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'admin'],
    ],
    function () {
        include_once 'admin/admin.php';
    }
);

/*===================================
===========< supporter Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'supporter'],
    ],
    function () {
        include_once 'supporter/supporter.php';
    }
);

/*===================================
===========< victim Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'victim'],
    ],
    function () {
        include_once 'victim/victim.php';
    }
);
