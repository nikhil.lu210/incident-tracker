<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incident extends Model
{
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    
    // all relation are below

    //relation with User one to many relationship
    public function get_victim(){
        return $this->belongsTo('App\User', 'victim', 'id');
    }

    //relation with User one to many relationship
    public function get_supporter(){
        return $this->belongsTo('App\User', 'supporter', 'id');
    }

    //relation with User one to many relationship
    public function get_admin(){
        return $this->belongsTo('App\User', 'admin', 'id');
    }
}
