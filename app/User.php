<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // all relation below

    //relation with Role many to one relationship
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    //relation with Incident many to one relationship
    public function victims()
    {
        return $this->hasMany('App\Models\Incident', 'victim', 'id');
    }

    //relation with Incident many to one relationship
    public function supporters()
    {
        return $this->hasMany('App\Models\Incident', 'supporter', 'id');
    }

    //relation with Incident many to one relationship
    public function admins()
    {
        return $this->hasMany('App\Models\Incident', 'admin', 'id');
    }

}
