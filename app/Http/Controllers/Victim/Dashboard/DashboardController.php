<?php

namespace App\Http\Controllers\Victim\Dashboard;

use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'victim']);
    }
    public function index()
    {
        $authUser = auth()->user();

        $datas['total_incident'] = Incident::where('victim', $authUser->id)->count();

        $datas['total_pending'] = Incident::where('victim', $authUser->id)
                                            ->where('status', -1)
                                            ->whereNull('assigned_at')
                                            ->whereNull('solved_at')
                                            ->count();

        $datas['total_active'] = Incident::where('victim', $authUser->id)
                                            ->where('status', 0)
                                            ->whereNotNull('assigned_at')
                                            ->whereNull('solved_at')
                                            ->count();

        $datas['total_solved'] = Incident::where('victim', $authUser->id)
                                            ->where('status', 1)
                                            ->whereNotNull('assigned_at')
                                            ->whereNotNull('solved_at')
                                            ->count();

        // dd($datas);

        return view('victim.dashboard.index', $datas);
    }
}
