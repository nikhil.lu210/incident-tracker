<?php

namespace App\Http\Controllers\victim\Incident;

use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class IncidentController extends Controller
{    
    public function __construct()
    {
        $this->middleware(['auth', 'victim']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = auth()->user();
        $incidents = Incident::with(['get_supporter'])->where('victim', $authUser->id)->get();
        return view('victim.incident.index', compact(['incidents']));
    }
    
    /**
     * create new incident page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('victim.incident.create');
    }
    
    /**
     * store incident data
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'                => 'required | string',
            'description'          => 'required | string',
        ]);

        $incident = new Incident();

        $incident_id = 'JAKINC-' . auth()->user()->id . strtotime(now());

        $incident->incident_id = $incident_id;
        $incident->title = $request->title;
        $incident->victim = auth()->user()->id;
        $incident->description = $request->description;
        $incident->status = -1;

        $incident->save();

        Session::flash('message', "Your Incident Has Been Submitted.");
        return Redirect::back();
    }
    

    /**
     * Show Incident
     */
    public function show($incident_id)
    {
        $authUser = auth()->user();
        $incident = Incident::with(['get_supporter'])->where('victim', $authUser->id)->findOrFail($incident_id);
        return view('victim.incident.show', compact(['incident']));
    }
}
