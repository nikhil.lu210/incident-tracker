<?php

namespace App\Http\Controllers\Victim\Profile;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'victim']);
    }
    public function index()
    {
        $victim = auth()->user();
        return view('victim.profile.index', compact(['victim']));
    }
    

    // Update Profile
    public function update(Request $request, $victim_id)
    {
        // dd($request);
        $victim = User::findOrFail($victim_id);

         $this->validate($request, array(
             'name'     => 'required | string',
             'email'    => 'required | email',
             'mobile'   => 'required | string',
         ));

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg | max:2048',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $victim->avatar = 'data:image/png;base64,'.$encoded_image;
        }
        
        if($request->name)
            $victim->name = $request->name;
        
        if($request->email)
            $victim->email = $request->email;
        
        if($request->mobile)
            $victim->mobile = $request->mobile;

        if($victim->save()){
            Session::flash('message', "Profile Updated.");
            return Redirect::back();
        } else {
            Session::flash('message', "Profile Not Updated.");
            return Redirect::back();
        }
    }


    /**
     * passwordUpdate
     */
    public function passwordUpdate(Request $request, $victim_id)
    {
        $victim = User::findOrFail(decrypt($victim_id));

        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'password'          => 'required | string | min:8 | same:password_confirmation | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            Session::flash('error', "Current Password Does Not Match...");
            return Redirect::back();
        }
        elseif($request->old_password == $request->password){
            Session::flash('error', "Both are same...");
            return Redirect::back();
        }
        else{
            $victim = Auth::user();

            $victim->password = Hash::make($request->password);

            if($victim->save()){
                Session::flash('message', "Password has been changed Successfully");
            } else{
                Session::flash('error', "Password Does not Changed");
            }
            
            return redirect()->back();
        }
    }
}
