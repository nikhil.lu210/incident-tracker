<?php

namespace App\Http\Controllers\Supporter\Profile;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'supporter']);
    }
    public function index()
    {
        $supporter = auth()->user();
        return view('supporter.profile.index', compact(['supporter']));
    }

    

    // Update Profile
    public function update(Request $request, $supporter_id)
    {
        // dd($request);
        $supporter = User::findOrFail($supporter_id);

         $this->validate($request, array(
             'name'     => 'required | string',
             'email'    => 'required | email',
             'mobile'   => 'required | string',
         ));

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg | max:2048',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $supporter->avatar = 'data:image/png;base64,'.$encoded_image;
        }
        
        if($request->name)
            $supporter->name = $request->name;
        
        if($request->email)
            $supporter->email = $request->email;
        
        if($request->mobile)
            $supporter->mobile = $request->mobile;

        if($supporter->save()){
            Session::flash('message', "Profile Updated.");
            return Redirect::back();
        } else {
            Session::flash('message', "Profile Not Updated.");
            return Redirect::back();
        }
    }


    /**
     * passwordUpdate
     */
    public function passwordUpdate(Request $request, $supporter_id)
    {
        $supporter = User::findOrFail(decrypt($supporter_id));

        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'password'          => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/ | same:password_confirmation',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            Session::flash('error', "Current Password Does Not Match...");
            return Redirect::back();
        }
        elseif($request->old_password == $request->password){
            Session::flash('error', "Both are same...");
            return Redirect::back();
        }
        else{
            $supporter = Auth::user();

            $supporter->password = Hash::make($request->password);

            if($supporter->save()){
                Session::flash('message', "Password has been changed Successfully");
            } else{
                Session::flash('error', "Password Does not Changed");
            }
            
            return redirect()->back();
        }
    }
}
