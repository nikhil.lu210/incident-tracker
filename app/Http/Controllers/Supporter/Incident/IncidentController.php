<?php

namespace App\Http\Controllers\Supporter\Incident;

use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class IncidentController extends Controller
{    
    public function __construct()
    {
        $this->middleware(['auth', 'supporter']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newAssigned()
    {
        $authUser = auth()->user();
        $incidents = Incident::where('supporter', $authUser->id)
                             ->where('status', 0)
                             ->whereNotNull('assigned_at')
                             ->whereNull('solved_at')
                             ->get();
        // dd($incidents);
        return view('supporter.incident.new_assigned', compact(['incidents']));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = auth()->user();
        $incidents = Incident::where('supporter', $authUser->id)
                             ->whereNotNull('assigned_at')
                             ->get();
        return view('supporter.incident.index', compact(['incidents']));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function solved()
    {
        $authUser = auth()->user();
        $incidents = Incident::where('supporter', $authUser->id)
                             ->where('status', 1)
                             ->whereNotNull('assigned_at')
                             ->whereNotNull('solved_at')
                             ->get();
        // dd($incidents);
        return view('supporter.incident.solved', compact(['incidents']));
    }
    

    /**
     * Show Incident
     */
    public function show($page, $incident_id)
    {
        $incident = Incident::with(['get_victim'])->findOrFail($incident_id);
        return view('supporter.incident.show', compact(['incident', 'page']));
    }
    

    /**
     * Mark As Solved
     */
    public function mark_as_solved(Request $request, $incident_id)
    {
        // dd($incident_id, $request);
        $this->validate($request, [
            'note' => 'required | string',
        ]);

        $incident = Incident::where('id', $incident_id)->first();

        $incident->note = $request->note;
        $incident->status = 1;
        $incident->solved_at = now();

        $incident->save();

        Session::flash('message', "Marked As Solved.");
        return Redirect::back();
    }
}
