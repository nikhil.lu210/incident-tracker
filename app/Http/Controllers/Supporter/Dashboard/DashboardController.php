<?php

namespace App\Http\Controllers\Supporter\Dashboard;

use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'supporter']);
    }
    public function index()
    {
        $authUser = auth()->user();

        $datas['total_incident'] = Incident::count();

        $datas['total_assigned'] = Incident::where('supporter', $authUser->id)->count();

        $datas['total_active'] = Incident::where('supporter', $authUser->id)
                                            ->where('status', 0)
                                            ->whereNotNull('assigned_at')
                                            ->whereNull('solved_at')
                                            ->count();

        $datas['total_solved'] = Incident::where('supporter', $authUser->id)
                                            ->where('status', 1)
                                            ->whereNotNull('assigned_at')
                                            ->whereNotNull('solved_at')
                                            ->count();

        // dd($datas);

        return view('supporter.dashboard.index', $datas);
    }
}
