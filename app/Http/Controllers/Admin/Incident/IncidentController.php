<?php

namespace App\Http\Controllers\Admin\Incident;

use App\User;
use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class IncidentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All Incident
     */
    public function index()
    {
        $incidents = Incident::with(['get_supporter', 'get_victim'])->get();
        // dd($incidents);
        return view('admin.incident.index', compact(['incidents']));
    }

    /**
     * Pending Incident
     */
    public function pending()
    {
        $incidents = Incident::where('status', -1)->with(['get_supporter', 'get_victim'])->get();
        // dd($incidents);
        return view('admin.incident.pending', compact(['incidents']));
    }

    /**
     * Running Incident
     */
    public function running()
    {
        $incidents = Incident::where('status', 0)->with(['get_supporter', 'get_victim'])->get();
        // dd($incidents);
        return view('admin.incident.running', compact(['incidents']));
    }

    /**
     * Solved Incident
     */
    public function solved()
    {
        $incidents = Incident::where('status', 1)->with(['get_supporter', 'get_victim'])->get();
        // dd($incidents);
        return view('admin.incident.solved', compact(['incidents']));
    }

    /**
     * Show Incident
     */
    public function show($page, $incident_id)
    {
        $incident = Incident::with(['get_supporter', 'get_victim'])->findOrFail($incident_id);
        $supporters = User::select(['id', 'role_id', 'name'])->where('role_id', 2)->get();
        // dd($supporters);
        return view('admin.incident.show', compact(['incident', 'page', 'supporters']));
    }


    /**
     * Assign Supporter
     */
    public function assign_supporter(Request $request, $incident_id)
    {
        // dd($incident_id, $request);
        $this->validate($request, [
            'supporter_id' => 'required | integer',
        ]);

        $incident = Incident::where('id', $incident_id)->first();

        $incident->supporter = $request->supporter_id;
        $incident->admin = auth()->user()->id;
        $incident->status = 0;
        $incident->assigned_at = now();

        $incident->save();

        Session::flash('message', "Supporter Assigned.");
        return Redirect::back();
    }
}
