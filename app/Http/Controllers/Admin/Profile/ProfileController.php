<?php

namespace App\Http\Controllers\Admin\Profile;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        $admin = auth()->user();
        return view('admin.profile.index', compact(['admin']));
    }

    // Update Profile
    public function update(Request $request, $admin_id)
    {
        // dd($request);
        $admin = User::findOrFail($admin_id);

         $this->validate($request, array(
             'name'     => 'required | string',
             'email'    => 'required | email',
             'mobile'   => 'required | string',
         ));

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg | max:2048',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $admin->avatar = 'data:image/png;base64,'.$encoded_image;
        }
        
        if($request->name)
            $admin->name = $request->name;
        
        if($request->email)
            $admin->email = $request->email;
        
        if($request->mobile)
            $admin->mobile = $request->mobile;

        if($admin->save()){
            Session::flash('message', "Profile Updated.");
            return Redirect::back();
        } else {
            Session::flash('error', "Profile Not Updated.");
            return Redirect::back();
        }
    }


    /**
     * passwordUpdate
     */
    public function passwordUpdate(Request $request, $admin_id)
    {
        $admin = User::findOrFail(decrypt($admin_id));

        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'password'          => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/ | same:password_confirmation',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            Session::flash('error', "Current Password Does Not Match...");
            return Redirect::back();
        }
        elseif($request->old_password == $request->password){
            Session::flash('error', "Both are same...");
            return Redirect::back();
        }
        else{
            $admin = Auth::user();

            $admin->password = Hash::make($request->password);

            if($admin->save()){
                Session::flash('message', "Password has been changed Successfully");
            } else{
                Session::flash('error', "Password Does not Changed");
            }
            
            return redirect()->back();
        }
    }
}
