<?php

namespace App\Http\Controllers\Admin\Administrator;

use App\User;
use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AdministratorController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All administrator
     */
    public function index()
    {
        $admins = User::where('role_id', 1)->get();
        // dd($admins);
        return view('admin.administrator.index', compact(['admins']));
    }

    /**
     * add administrator
     */
    public function create()
    {
        return view('admin.administrator.create');
    }

    

    /**
     * store admin
     */
    public function store(Request $request)
    {
        // dd($request);
        
        $this->validate($request, array(
            'name'     => 'required | string',
            'email'    => 'required | email',
            'mobile'   => 'required | string',
            'password' => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
        ));
        
        $admin = new User();

        if($request->hasFile('avatar')){
            
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg | max:2048',
            ));
            
            $image_data=file_get_contents($request->avatar);
            
            $encoded_image=base64_encode($image_data);

            $admin->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        $admin->role_id = 1;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->password = Hash::make($request->password);

        $admin->save();

        if($admin->save()){
            Session::flash('message', "New admin Created.");
            return Redirect::back();
        } else {
            Session::flash('error', "admin Not Created.");
            return Redirect::back();
        }
    }

    /**
     * Show admin
     */
    public function show($admin_id)
    {
        $admin = User::with('admins')->findOrFail($admin_id);
        $incidents = Incident::where('admin', $admin_id)->get();
        return view('admin.administrator.show', compact(['admin', 'incidents']));
    }
}
