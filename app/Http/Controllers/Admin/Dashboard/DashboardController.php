<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Incident;
use App\User;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }
    public function index()
    {
        $datas['total_admin'] = User::where('role_id', 1)->count();
        $datas['total_supporter'] = User::where('role_id', 2)->count();
        $datas['total_victim'] = User::where('role_id', 3)->count();
        $datas['total_incident'] = Incident::count();

        return view('admin.dashboard.index', $datas);
    }
}
