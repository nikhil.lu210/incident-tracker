<?php

namespace App\Http\Controllers\Admin\Victim;

use App\User;
use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VictimController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All victim
     */
    public function index()
    {
        $victims = User::where('role_id', 3)->with(['victims'])->get();
        // dd($victims);
        return view('admin.victim.index', compact(['victims']));
    }

    /**
     * Show victim
     */
    public function show($victim_id)
    {
        $victim = User::with('victims')->findOrFail($victim_id);
        $incidents = Incident::where('victim', $victim_id)->get();
        return view('admin.victim.show', compact(['victim', 'incidents']));
    }
}
