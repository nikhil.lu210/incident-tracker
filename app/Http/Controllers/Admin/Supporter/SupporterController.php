<?php

namespace App\Http\Controllers\Admin\Supporter;

use App\User;
use App\Models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class SupporterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All supporter
     */
    public function index()
    {
        $supporters = User::where('role_id', 2)->with(['supporters'])->get();
        // dd($supporters);
        return view('admin.supporter.index', compact(['supporters']));
    }

    /**
     * add supporter
     */
    public function create()
    {
        return view('admin.supporter.create');
    }

    /**
     * store supporter
     */
    public function store(Request $request)
    {
        // dd($request);
        
        $this->validate($request, array(
            'name'     => 'required | string',
            'email'    => 'required | email',
            'mobile'   => 'required | string',
            'password' => 'required | string | min:8 | regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
        ));
        
        $supporter = new User();

        if($request->hasFile('avatar')){
            
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg | max:2048',
            ));
            
            $image_data=file_get_contents($request->avatar);
            
            $encoded_image=base64_encode($image_data);

            $supporter->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        $supporter->role_id = 2;
        $supporter->name = $request->name;
        $supporter->email = $request->email;
        $supporter->mobile = $request->mobile;
        $supporter->password = Hash::make($request->password);

        $supporter->save();

        if($supporter->save()){
            Session::flash('message', "New Supporter Created.");
            return Redirect::back();
        } else {
            Session::flash('error', "Supporter Not Created.");
            return Redirect::back();
        }
    }

    /**
     * Show supporter
     */
    public function show($supporter_id)
    {
        $supporter = User::with('supporters')->findOrFail($supporter_id);
        $incidents = Incident::where('supporter', $supporter_id)->get();

        // dd($incidents);
        return view('admin.supporter.show', compact(['supporter', 'incidents']));
    }
}
