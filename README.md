## Incident Tracker Management

This is a simple Incident Tracker Management System App made with Laravel framework. Developer By [Nikhil Kurmi](https://gitlab.com/nikhil.lu210)

## Template

This application made with [ZiVi Admin Dashboard](http://radixtouch.in/templates/admin/zivi/source/light/index.html) template.
